from django.contrib import admin
from django.db.models import Count
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from emenu import models
from modeltranslation.admin import TranslationAdmin


class RowActionsAdmin(admin.ModelAdmin):
    """
    Base class for admin row action's
    add the delete and edit buttons to every row in changelist.
    """

    def delete_url(self, obj):
        info = obj._meta.app_label, obj._meta.model_name
        return reverse('admin:%s_%s_delete' % info, args=(obj.id,))

    def edit_url(self, obj, custom_url=None):
        if custom_url:
            return custom_url
        info = obj._meta.app_label, obj._meta.model_name
        return reverse('admin:%s_%s_change' % info, args=(obj.id,))

    def get_list_display(self, request, delete_action=True, row_actions=True):
        info = self.model._meta.app_label, self.model._meta.model_name
        delete_permission = '%s.delete_%s' % info
        if request.user.has_perm(delete_permission) and delete_action:
            return list(self.list_display) + ['row_actions_with_delete']
        if row_actions:
            return list(self.list_display) + ['row_actions']
        return list(self.list_display)

    def row_actions(self, obj):
        edit_icon = ' <i class="material-icons">edit</i>'
        template = '<a href="%s"><span class="i-blue">%s</span></a>'
        return mark_safe(template % (self.edit_url(obj), edit_icon))

    row_actions.short_description = _('Actions')

    def row_actions_with_delete(self, obj):
        delete_icon = ' <i class="material-icons">delete_forever</i>'
        template = '<a href="%s"><span class="i-red">%s </span></a>'
        return mark_safe('\n'.join([
            template % (self.delete_url(obj), delete_icon),
            self.row_actions(obj)
        ]))

    row_actions_with_delete.short_description = _('Actions')


class ImagePreviewAdmin(object):
    def preview(self, obj):
        template = u"""<img src="{url}" style="max-height: {size};" />"""
        config = {
            'field': 'image',
            'size': '40px',
        }
        custom_config = getattr(self, 'set_preview', {})
        config.update(custom_config)
        image = getattr(obj, config['field'], None)
        url = image.url if image else ''
        return mark_safe(
            template.format(
                url=url,
                size=config['size'])
        )

    preview.short_description = _('preview')
    preview.allow_tags = True


@admin.register(models.Category)
class CategoryAdmin(TranslationAdmin, RowActionsAdmin, ):
    list_display_links = ['id', ]
    list_display = ('id', 'title',)
    list_editable = ('title',)


@admin.register(models.Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display_links = []
    list_display = ('id', 'code', 'name', 'symbol')
    list_editable = ('code', 'name', 'symbol')


@admin.register(models.Price)
class PriceAdmin(admin.ModelAdmin):
    list_select_related = (
        'dish',
        'currency',
    )
    list_display_links = ['id', 'dish']
    list_display = (
        'id',
        'dish',
        'price',
        'currency',
    )
    list_editable = (
        'price',
        'currency',
    )
    list_filter = ('currency',)


class PriceInlineAdmin(admin.TabularInline):
    model = models.Price
    extra = 0
    can_delete = True
    show_change_link = True


class BaseAdmin(
    TranslationAdmin,
    ImagePreviewAdmin,
    RowActionsAdmin,
):
    """
    Base class for "MenuCard" and "Dish" model's
    """
    readonly_fields = ['created_at', 'updated_at']
    search_fields = ('name', 'description',)
    list_per_page = 10

    class Media:
        css = {'all': ('emenu/css/style.css',)}


@admin.register(models.MenuCard)
class MenuCardAdmin(BaseAdmin):
    list_display = (
        'id',
        'name',
        'is_active',
        'count',
        'preview',
        'image',
        'updated_at',
    )
    list_display_links = ('id',)
    list_editable = (
        'name',
        'image',
        'is_active',
    )
    list_filter = ('is_active',)
    list_per_page = 10

    def count(self, obj):
        return obj.dishes_count

    count.admin_order_field = 'dishes_count'
    count.short_description = _('dishes count')
    count.allow_tags = True

    def get_queryset(self, request):
        """
        add a sortable dish count column
        """
        queryset = super(MenuCardAdmin, self).get_queryset(request)
        if not self.has_change_permission(request):
            queryset = queryset.none()
        return queryset.annotate(dishes_count=Count('dishes'))


@admin.register(models.Dish)
class DishAdmin(BaseAdmin):
    list_select_related = True
    inlines = [PriceInlineAdmin]
    list_display = (
        'id',
        'name',
        'preview',
        'is_active',
        'is_vegetarian',
        'preparation_time',
        'category',
        'price',
    )
    list_display_links = ('id',)
    list_editable = (
        'name',
        'is_active',
        'is_vegetarian',
        'preparation_time',
        'category',
    )
    list_filter = (
        'is_active',
        'is_vegetarian',
        'category',
    )

    def price(self, obj):
        text = ''
        for price in obj.prices.all():
            text += """<a href="%s">%s </a>""" % (
                price.get_absolute_url(),
                price
            )
        return mark_safe(text)
