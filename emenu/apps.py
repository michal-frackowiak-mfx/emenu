from django.apps import AppConfig

from material.frontend.apps import ModuleMixin


class EmenuConfig(ModuleMixin, AppConfig):
    name = 'emenu'
    default_app_config = 'emenu.apps.EmenuConfig'
    label = 'emenu'
    icon = '<i class="material-icons">flight_takeoff</i>'
