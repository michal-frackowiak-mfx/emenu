"""
#----------------------------------+
| load demo content from fixture's |
#----------------------------------+
"""
from __future__ import (
    unicode_literals,
    print_function
)

from django.contrib.auth.models import User
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import IntegrityError

fixtures = [
    'user',
    'category',
    'dish',
    'currency',
    'price',
    'menucard',
]


class Command(BaseCommand):
    def create_admin_user(self, username, password):
        u = User(username=username)
        u.set_password(password)
        u.is_superuser = True
        u.is_staff = True
        u.save()

    def success(self, text):
        self.stdout.write(self.style.SUCCESS(text))

    def error(self, text):
        self.stdout.write(self.style.WARNING(text))

    def handle(self, **options):
        self.success('Django started loading demo!')
        self.success('migrate...')

        call_command(
            'makemigrations',
            no_input=True,
            interactive=False
        )
        call_command(
            'migrate',
            no_input=True,
            interactive=False
        )
        self.success('start loading fixtures')

        for fixture in fixtures:
            self.success('  loading: %s' % fixture)
            call_command('loaddata', fixture)
        try:
            self.create_admin_user('admin', 'rekrut2018')
            self.stdout.write(
                'admin-user created!\n login:%s\n pass:%s' % (
                    self.style.SUCCESS('admin'),
                    self.style.SUCCESS('admin'),
                )
            )
        except IntegrityError:
            self.stdout.write(
                self.style.WARNING(
                    'user "admin" already exists.'
                )
            )
        self.stdout.write(
            'Demo content loaded %s' % (
                self.style.SUCCESS('successfully!')
            )
        )
        self.success('starting server...')
        call_command('runserver')
