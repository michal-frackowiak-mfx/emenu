from django.conf.urls import (url, include)
from emenu import views

app_name = 'emenu'
urlpatterns = [
    url(r'^$', views.EmenuView.as_view(), name='index'),
    url(r'^login/', views.LoginView.as_view(), name='login'),
    url(r'^logout/', views.LogoutView.as_view(), name='logout'),
    url(r'^menucard/', include(views.MenuCardViewSet().urls)),
    url(r'^dish/', include(views.DishViewSet().urls)),
    url(r'^category/', include(views.CategoryViewSet().urls)),
    url(r'^price/', include(views.PriceViewSet().urls)),
    url(
        r'^(?P<menucard_id>[0-9]+)/$',
        views.DishListView.as_view(),
        name='menu'
    ),
    url(
        r'^dish_nr/(?P<dish_id>[0-9]+)/$',
        views.DishDetailView.as_view(),
        name='dish_id'
    ),
    url(
        r'^(?P<menucard_id>[0-9]+)/dish_nr/(?P<dish_id>[0-9]+)/$',
        views.DishDetailView.as_view(),
        name='menu_dish'
    ),
]
