from django.db import models
from django.db.models import Count
from django.utils.translation import gettext as _
from django.utils.translation import ugettext_lazy as _
from material import (
    Layout,
    Row,
    Fieldset
)
from material.frontend.views import (
    ModelViewSet,
)

from emenu.admin import (
    ImagePreviewAdmin,
)
from emenu.forms import (
    MenuCardForm,
    CategoryForm,
    DishForm,

)
from emenu.models import (
    MenuCard,
    Dish,
    Category,
    Currency,
    Price
)


class MenuCardViewSet(
    ImagePreviewAdmin,
    ModelViewSet
):
    model = MenuCard
    form_class = MenuCardForm

    ordering = ['id']
    list_display = (
        'id',
        'name',
        'is_active',
        'dish_count',
        'preview',
        'updated_at',
        'created_at',
    )
    list_display_links = ('id', 'name',)
    list_filter = ('is_active',)
    list_per_page = 10
    queryset = MenuCard.objects.all().annotate(
        dish_count=Count('dishes')
    )
    layout = Layout(
        Fieldset(
            _('Menu'),
            Row('name_en', 'name_pl'),
            'is_active',
            Row('description_en', 'description_pl')
        ),
    )

    def dish_count(self, obj):
        return obj.dish_count

    dish_count.admin_order_field = 'dish_count'
    dish_count.short_description = _('dishes')
    dish_count.allow_tags = True


class DishViewSet(ImagePreviewAdmin, ModelViewSet):
    model = Dish
    form_class = DishForm
    list_per_page = 1
    ordering = ['name']
    list_display = (
        'id',
        'name',
        'preview',
        'is_active',
        'is_vegetarian',
        'preparation_time',
        'category',
    )
    list_display_links = ('id',)
    list_filter = (
        'is_active',
        'is_vegetarian',
        'category',
    )


class CategoryViewSet(ModelViewSet):
    model = Category
    form_class = CategoryForm
    ordering = ['id']
    list_display_links = ['id', 'title_en', 'title_pl', ]
    list_display = ('id', 'title_en', 'title_pl', 'dish_count')
    queryset = Category.objects.all().annotate(
        dish_count=Count('dish')
    )
    layout = Layout(
        Fieldset(
            _('category name:'),
            Row('title_en', 'title_pl'),
        )
    )

    def dish_count(self, obj):
        return obj.dish_count

    dish_count.admin_order_field = 'dish_count'
    dish_count.short_description = _('dishes in category')
    dish_count.allow_tags = True


class CurrencyViewSet(ModelViewSet):
    model = Currency
    ordering = ['id']
    list_display_links = ['id', ]
    list_display = ('id', 'code', 'name', 'symbol')
    list_editable = ('code', 'name', 'symbol')

    dish = models.ForeignKey(
        'Dish',
        verbose_name=_('dish'),
        blank=True,
        on_delete=models.CASCADE,
        related_query_name='price',
        related_name='prices',
    )
    currency = models.ForeignKey(
        Currency,
        verbose_name=_('currency'),
        blank=True,
        on_delete=models.CASCADE,
        related_name='price'
    )


class PriceViewSet(ModelViewSet):
    model = Price
    list_display_links = [
        'price',
        'currency',
        'dish'
    ]
    list_display = (
        'dish',
        'price',
        'currency',
    )
