from django.db.models import Count
from django.shortcuts import (
    get_object_or_404
)
from django.views.generic import (
    TemplateView,
    ListView
)
from material.frontend.views import (
    DetailModelView
)

from emenu.models import (
    MenuCard,
    Dish
)


class LogoutView(TemplateView):
    template_name = 'emenu/registration/logged_out.html'


class LoginView(TemplateView):
    template_name = 'emenu/registration/login.html'


class CustomListOrdering(object):
    """
    List ordering handler for generic List Views
    """

    def get_ordering(self):
        ordering_dict = dict(self.request.GET.items())
        self.order = ordering_dict.get('order', 'asc')
        self.ordering = ordering_dict.get('ordering', 'id')
        self.page = ordering_dict.get('page', '1')
        if self.order == 'desc':
            self.ordering = '-%s' % self.ordering
        return self.ordering


class EmenuView(CustomListOrdering, ListView):
    """
    [ e-Menu home page - public visible page ]
    show only "active" and not empty "MenuCards"
    """
    model = MenuCard
    template_name = 'emenu/index.html'
    paginate_by = 3
    queryset = MenuCard.objects.not_empty().active().annotate(
        dish_count=Count('dishes')
    )
    context_object_name = "object_list"

    def get_context_data(self, *args, **kwargs):
        context = super(EmenuView, self).get_context_data(*args, **kwargs)
        context['current_order'] = self.get_ordering()
        context['order'] = self.order
        context['query_count'] = self.queryset.count()
        return context


class DishListView(CustomListOrdering, ListView):
    model = Dish
    template_name = 'emenu/dish_list_page.html'
    paginate_by = 3
    queryset = Dish.objects.all()

    def get_context_data(self, **kwargs):
        context = super(
            DishListView, self
        ).get_context_data(**kwargs)
        context.update({
            'menucard': get_object_or_404(
                MenuCard,
                pk=self.kwargs.get('menucard_id')
            ),
            'current_order': self.get_ordering(),
            'order': self.order,
            'query_count': self.queryset.count(),

        })
        return context


class DishDetailView(DetailModelView):
    model = Dish
    template_name = 'emenu/dish_detail_page.html'

    def get_context_data(self, **kwargs):
        context = super(DishDetailView, self).get_context_data(**kwargs)
        context.update({
            'menucard_id': kwargs.get('menucard_id')
        })
        return context

    def get_object(self):
        self.object = get_object_or_404(
            Dish,
            id=self.kwargs.get('dish_id')
        )
        return self.object
