from modeltranslation.translator import (
    translator,
    TranslationOptions
)

from emenu import models


class BaseTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


class CategoryTranslationOptions(TranslationOptions):
    fields = ('title',)


translator.register(models.Dish, BaseTranslationOptions)
translator.register(models.MenuCard, BaseTranslationOptions)
translator.register(models.Category, CategoryTranslationOptions)
