from django.conf import settings
from django.test import TestCase


class EmenuLanguageTests(TestCase):
    def test_language_using_cookie(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: 'en'})
        response = self.client.get('/', follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'nie znaleziono kart menu!')

        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: 'pl'})

        response = self.client.get('/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'nie znaleziono kart menu!')

    def test_language_using_header(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: 'en'})
        response = self.client.get('/', HTTP_ACCEPT_LANGUAGE='en', follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'nie znaleziono kart menu!')

        response = self.client.get('/', HTTP_ACCEPT_LANGUAGE='pl', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'nie znaleziono kart menu!')
