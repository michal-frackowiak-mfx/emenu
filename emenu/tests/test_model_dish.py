from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from emenu.models import *  # noqa


class TestDishModel(TestCase):
    def create_dish(self):
        dish = Dish.objects.create(
            name='TESTDISH',
            name_pl='TESTDISH_PL',
            name_en='TESTDISH_EN',
            is_vegetarian=True,
            is_active=True,
            created_at=timezone.now(),
            updated_at=timezone.now(),
            category=None,
        )
        dish.save()
        return dish

    def test_dish_creation(self):
        w = self.create_dish()
        self.assertTrue(isinstance(w, Dish))
        self.assertEqual(w.__str__(), w.name)

    def test_model_manager(self):
        call_command('loaddata', 'user')
        call_command('loaddata', 'category')
        call_command('loaddata', 'dish')
        count = Dish.objects.all().count()
        self.assertTrue(
            count >= Dish.objects.all().filter(
                is_active=True).count()
        )

    def test_create_currency(self):
        call_command('loaddata', 'user')
        call_command('loaddata', 'category')
        call_command('loaddata', 'dish')
        prices_count = Price.objects.all().count()
        currencies_count = Currency.objects.all().count()

        currency, result = Currency.objects.get_or_create(
            name='DollarTest',
            symbol='$$',
            code='USDTEST'
        )
        dish = Dish.objects.first()
        price = Price.objects.create(
            dish=dish,
            currency=currency,
            price=Decimal(1)
        )
        price.save()
        self.assertEqual(
            currencies_count + 1,
            Currency.objects.all().count()
        )
        self.assertEqual(
            prices_count + 1,
            Price.objects.all().count()
        )

        for dish in Dish.objects.all():
            self.assertTrue(len(dish.name_pl) > 0)
            self.assertTrue(len(dish.name_en) > 0)
            self.assertTrue(dish.id > 0)
