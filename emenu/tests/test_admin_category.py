from django.core.management import call_command
from django.test import TestCase, Client
from emenu.models import Category


class ModelAdminTests(TestCase):
    def create_category(self):
        self.client = Client()
        self.client.login(
            username='testadmin',
            password='rekrut2018'
        )
        self.client.post(
            '/admin/emenu/category/add/',
            {
                'title': 'test',
                'title_en': 'test_en',
                'title_pl': 'test_pl',
            },
            follow=True,
        )
        self.client.logout()

    def setUp(self):
        call_command('loaddata', 'user')
        call_command('loaddata', 'category')

    def test_admin_create_category(self):
        count = Category.objects.all().count()
        self.create_category()
        all = Category.objects.all()
        category = all.last()
        self.assertEquals(all.count(), count+1)
        self.assertTrue(category)
        self.assertEquals(category.title_pl, 'test_pl')
