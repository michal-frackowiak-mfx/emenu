from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase, Client
from emenu.models import *  # noqa


class TestDjangoAdminPanel(TestCase):
    """
    Admin multiple views tests
    """

    def create_user(self):
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user

    def test_spider_admin(self):
        call_command('loaddata', 'category')
        call_command('loaddata', 'dish')
        call_command('loaddata', 'menucard')
        call_command('loaddata', 'currency')
        call_command('loaddata', 'price')
        self.create_user()
        client = Client()
        client.login(username=self.username, password=self.password)
        admin_pages = [
            "/admin/",
            "/admin/emenu/category/",
            "/admin/emenu/category/add/",
            "/admin/emenu/category/1/change/",
            "/admin/emenu/menucard/",
            "/admin/emenu/menucard/add/",
            "/admin/emenu/menucard/1/change/",
            "/admin/emenu/menucard/2/change/",
            "/admin/emenu/menucard/3/change/",
            "/admin/emenu/menucard/4/change/",
            "/admin/emenu/dish/",
            "/admin/emenu/dish/add/",
            "/admin/emenu/dish/1/change/",
            "/admin/emenu/price/",
            "/admin/emenu/price/add/",
            "/admin/emenu/price/1/change/",
            "/admin/emenu/currency/",
            "/admin/emenu/currency/add/",
            "/admin/emenu/currency/1/change/",
            "/admin/auth/",
            "/admin/auth/group/",
            "/admin/auth/group/add/",
            "/admin/auth/user/",
            "/admin/auth/user/add/",
            "/admin/password_change/"
        ]
        for page in admin_pages:
            response = client.get(page)
            self.assertEquals(response.status_code, 200)
            self.assertContains(response, 'emenu/css/style.css')
