from django.core.management import call_command
from django.test import TestCase, Client
from emenu.models import MenuCard


class ModelAdminTests(TestCase):
    def create_menucard(self):
        self.client = Client()
        self.client.login(username='testadmin', password='rekrut2018')
        self.client.post(
            '/admin/emenu/menucard/add/',
            {
              'name': 'TEST',
              'name_en': 'TEST_EN',
              'name_pl': 'TEST_PL',
              'description_en': '',
              'description_pl': '',
              'is_active': True,
              'dishes': []
            },
            follow=True,
        )
        self.client.logout()

    def setUp(self):
        call_command('loaddata', 'user')
        call_command('loaddata', 'menucard')

    def test_admin_create_category(self):
        count = MenuCard.objects.all().count()
        self.create_menucard()

        self.assertEquals(
            MenuCard.objects.all().count(),
            count+1
        )
        self.assertEquals(
            MenuCard.objects.last().name_pl,
            'TEST_PL'
        )
        self.assertEquals(
            MenuCard.objects.last().name_en,
            'TEST_EN'
        )
