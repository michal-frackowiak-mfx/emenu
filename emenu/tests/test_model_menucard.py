from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from emenu.models import MenuCard


class TestMenuCardModel(TestCase):
    def create_menucard(self):
        return MenuCard.objects.create(
            name='TESTMENU',
            name_pl='TESTMENU_PL',
            name_en='TESTMENU_EN',
            created_at=timezone.now(),
            updated_at=timezone.now(),
        )

    def test_menucard_creation(self):
        w = self.create_menucard()
        self.assertTrue(isinstance(w, MenuCard))
        self.assertEqual(w.__str__(), w.name)

    def test_model_manager(self):
        call_command('loaddata', 'user')
        call_command('loaddata', 'category')
        call_command('loaddata', 'dish')
        call_command('loaddata', 'menucard')
        all = MenuCard.objects
        count = all.count()
        active = MenuCard.objects.is_active().count()
        not_empty = MenuCard.objects.not_empty().count()
        self.assertTrue(count >= active)
        self.assertEqual(0, not_empty)
