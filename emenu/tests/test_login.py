from django.conf import settings
from django.core.management import call_command
from django.test import Client
from django.test import TestCase
from django.urls import reverse


class EmenuUserLoginTests(TestCase):

    def test_no_user_exist(self):
        """
        Try to login if user not exists
        """
        response = self.client.get(reverse('emenu:index'))
        self.assertEqual(response.status_code, 200)

        self.client = Client()
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: 'pl'})
        response = self.client.post(
            '/accounts/login/',
            {'username': 'admin', 'password': 'FALSE'},
            follow=True
        )

        self.assertEqual(response.status_code, 200)

        # No redirected to index means not logged in!
        self.assertNotContains(response, 'nie znaleziono kart menu!')

    def test_success_login(self):
        """
        login successfully
        """
        call_command('loaddata', 'user')
        self.client = Client()
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: 'pl'})

        response = self.client.post(
            '/accounts/login/',
            {'username': 'testadmin', 'password': 'rekrut2018'},
            follow=True
        )

        self.assertEqual(response.status_code, 200)

        # If redirected to index then successfully logged in!
        self.assertContains(response, 'nie znaleziono kart menu!')
