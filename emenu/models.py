from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _


class ImagePreview(object):
    def preview(self, obj):
        template = u"""<img src="{url}" style="max-height: {size};" />"""
        config = {
            'field': 'image',
            'size': '48px',
        }
        custom_config = getattr(self, 'set_preview', {})
        config.update(custom_config)
        image = getattr(obj, config['field'], None)
        url = image.url if image else ''
        return mark_safe(
            template.format(
                url=url,
                size=config['size'])
        )


class DateTimeInfoAbstract(models.Model):
    """
    abstract base class with auto generated fields
    """
    created_at = models.DateTimeField(
        _('created at'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        _('updated at'),
        auto_now=True
    )

    class Meta:
        abstract = True


class BaseInfoQuerySet(models.QuerySet):
    def active(self):
        """
        get list of active objects
        """
        return self.filter(active=True)


class BaseInfoAbstract(DateTimeInfoAbstract):
    objects = BaseInfoQuerySet.as_manager()
    name = models.CharField(
        _('name'),
        max_length=128,
        null=True,
        blank=True,
        unique=True
    )
    description = models.CharField(
        _('description'),
        max_length=650,
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        _('active'),
        default=False
    )

    class Meta:
        app_label = 'emenu'
        abstract = True


class Currency(models.Model):
    code = models.CharField(
        _('code'),
        max_length=3,
        unique=True
    )
    name = models.CharField(
        _('name'),
        max_length=35
    )
    symbol = models.CharField(
        _('symbol'),
        max_length=4,
        blank=True
    )

    def __str__(self):
        return self.symbol

    class Meta:
        app_label = 'emenu'
        verbose_name = _('Currency')
        verbose_name_plural = _('Currencies')


class Price(models.Model):
    dish = models.ForeignKey(
        'Dish',
        verbose_name=_('dish'),
        blank=True,
        on_delete=models.CASCADE,
        related_query_name='price',
        related_name='prices',
    )
    currency = models.ForeignKey(
        Currency,
        verbose_name=_('currency'),
        blank=True,
        on_delete=models.CASCADE,
        related_name='price'
    )
    price = models.DecimalField(
        _('price'),
        default=0,
        decimal_places=2,
        max_digits=15,
        validators=[MinValueValidator(Decimal(0))],
        null=True,
        blank=True
    )

    def __str__(self):
        if self.currency:
            return u'%s %s' % (
                self.price,
                self.currency.symbol
            )
        else:
            return self.price

    class Meta:
        app_label = 'emenu'
        verbose_name = _('Price')
        verbose_name_plural = _('Prices')
        unique_together = ('dish', 'currency')

    def get_absolute_url(self):
        return reverse('emenu:price_change', kwargs={'pk': self.pk})


class Category(models.Model):
    title = models.CharField(
        _('title'),
        max_length=50,
        unique=True,
    )

    def __str__(self):
        return self.title

    class Meta:
        abstract = False
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Dish(BaseInfoAbstract, ImagePreview):
    is_vegetarian = models.BooleanField(
        _('vegetarian'),
        default=False
    )
    preparation_time = models.PositiveSmallIntegerField(
        _('preparation time'),
        default=10
    )
    image = models.ImageField(
        _('image'),
        upload_to='dishes/',
        default='/emenu/no-image.png',
    )
    category = models.ForeignKey(
        Category,
        verbose_name=_('category'),
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_query_name='dish',
        related_name='dishes'
    )

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'emenu'
        abstract = False
        verbose_name = _('Dish')
        verbose_name_plural = _('Dishes')


class MenuCardManagerQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def not_empty(self):
        return self.exclude(dishes=None)


class MenuCardManager(models.Manager):
    def get_queryset(self):
        return MenuCardManagerQuerySet(
            self.model, using=self._db
        )

    def is_active(self):
        return self.get_queryset().active()

    def not_empty(self):
        return self.get_queryset().not_empty()


class MenuCard(BaseInfoAbstract):
    objects = MenuCardManager()

    image = models.ImageField(
        _('image'),
        upload_to='cards/',
        default='emenu/no-image.png',
    )
    dishes = models.ManyToManyField(
        Dish,
        verbose_name=_('dishes'),
        related_name='cards',
        related_query_name='card',
        blank=True
    )

    class Meta:
        app_label = 'emenu'
        abstract = False
        verbose_name = _('Menu Card')
        verbose_name_plural = _('Menu Card\'s')

    def get_absolute_url(self):
        return reverse('menucard', kwargs={'id': self.id})

    def __str__(self):
        return self.name
