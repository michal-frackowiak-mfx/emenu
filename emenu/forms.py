from django import forms

from emenu import models


class CategoryForm(forms.ModelForm):

    class Meta:
        model = models.Category
        fields = (
            'title_en',
            'title_pl',
        )


class MenuCardForm(forms.ModelForm):

    class Meta:
        model = models.MenuCard
        fields = (
            'name_en',
            'description_en',
            'name_pl',
            'description_pl',
            'is_active',
            'image',
        )


class DishForm(forms.ModelForm):

    class Meta:
        model = models.Dish
        fields = (
            'name_en',
            'description_en',
            'name_pl',
            'description_pl',
            'category',
            'is_vegetarian',
            'is_active',
            'image'
        )
