# blue e-Menu

###  a digital "Menu" for restaurant's

This is a sample demo project created for need's of recruitment process.

recruitment for the position: "Python Developer" in company Blue Services

candidate: Michał Frąckowiak http://mfx.ovh

---
live demo with loaded fixtures:

- Homepage: http://mfx.ovh:8000/emenu/
- Django admin: http://mfx.ovh:8000/admin/
- Materialized Admin: http://mfx.ovh:8000/accounts/login/

---

## about eMenu project:

#### eMenu is a django based project, main features:

* extended django admin panel
* additional materialized administration panel
* multilingual (default: PL & EN)
* PEP8 - valid
* unit & integration tests
* front based on material-design

additional features:

* a category model for dish types
* image field for MenuCards and Dishes
* currency model for localisation purpose
* fixtures and easy demo load


---

### requirements:

 * python==3.6
 * django==2.0.5
 * Pillow==5.1.0
 * django-modeltranslation==0.13b1
 * django-material==1.2.5

#### requirement's for running test:

* django-webtest==1.9.3
* coverage==4.5.1
* django-nose==1.4.5
* django-test-pep8==0.1

### installation of requirement's:

The suggested way is to install the project in a virtual environment, like "virtualenv"

1.) create a virtual enviroment with python 3 inside. example:

```
virtualenv emenu --python=python36
```

2.) activate created the created virtual env:

```
source emenu/bin/activate   <- for linux based OS

emenu/Scripts/activate   <- for win based OS
```

3.) clone the repo:

```
git clone https://michal-frackowiak-mfx@bitbucket.org/michal-frackowiak-mfx/emenu.git
```


4.) install requirement's via pip:

```
cd emenu
pip install -r requirements.txt
```


### start fresh project:

This is standard django project you can build it in many way's!

The classic approach:

```
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```


#### loading fixture data:

```
python manage.py loaddata user category menucard dish currency price
```


#### loading demo:

You can setup a demo project with a management command:

```
python manage.py loaddemo
```

The loaddemo command is migrating the db and loading fixtures data into it.
Be carefully with this command in production!
The command can override some existing in db data!

---

### Databases

Default eMenu creates a sqlite3 DB for the demo purpose.
Tests are based also on sqlite3 DB.

BUT FOR PRODUCTION you should use a PostgresDB!

installation:

```
pip install psycopg2

```

and then in settings:

```

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'myproject',
        'USER': 'myprojectuser',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
    }
}

```

---
### running tests:

```
python manage.py test --settings=sampleproject.settings.test
```

tests are runned with "nose" unit test runner:

    * unit and integration tests
    * PEP8 test
    * coverage test

current coverage output checked with coverage==4.5.1:

```

Name                                    Stmts   Miss  Cover
-----------------------------------------------------------
emenu\__init__.py                           0      0   100%
emenu\admin.py                            103      5    95%
emenu\apps.py                               7      0   100%
emenu\forms.py                             15      0   100%
emenu\management\__init__.py                0      0   100%
emenu\management\commands\__init__.py       0      0   100%
emenu\migrations\0001_initial.py            9      0   100%
emenu\migrations\__init__.py                0      0   100%
emenu\models.py                           101     10    90%
emenu\translation.py                        9      0   100%
emenu\urls.py                               4      0   100%
emenu\views\__init__.py                     2      0   100%
emenu\views\administration.py              57      2    96%
emenu\views\public.py                      50      9    82%
-----------------------------------------------------------
TOTAL                                     357     26    93%
----------------------------------------------------------------------
Ran 16 tests in 5.160s

OK
Destroying test database for alias 'default'...

```


---

### translation's:

The project is currently available in languages: "PL" & "EN"
but you can easy add more languages!
to add a new language to project for example "de":

1.) add the language to settings, to base.py to the LANGUAGES tuple:

```
LANGUAGES = (
    ('en', ugettext('English')),
    ('pl', ugettext('Polish')),
    ('de', ugettext('German')),
)
```


2.) make migrations and migrate for creating of some columns for the newly added language:

```
 python manage.py makemigrations
 python manage.py migrate
```


3.) generate translation files for the newlycreated language:

```
cd emenu
 python manage.py makemessages -l de
```

4.) in locale folder is a .po file created for the german translation's.
After adding the translation's to the .po you should compile them with:

```
 python manage.py compilemessages
```

5.) The above command generates the compilled translation files ".po",
the german language is added to the eMenu app. you are done!


---

---


# ZADANIA TEKSTOWE:

## ZADANIE 1

suma ciągu arytmetycznego do kwadratu minus wzór na sumę kwadratów :)


```
def calculate(n):
    print(math.pow((((1 + float(n)) / 2) * float(n)), 2)-((n*(n+1)*(2*n+1))/6))

calculate(100)
```


## ZADANIE 2

optymalizacja przy założeniu że: wyprintowany w terminalu output musi być identyczny przed i po optymalizacji.


```
num_dict = {}
for nr, n in enumerate(range(1, 100)):
    num_dict.update({nr: n})
    print('%s\nIlosc liczb parzystych: %s\n%s' % (
        num_dict,
        str(int(num_dict.__len__() / 2)),
        'Size = None'
    ))
```
