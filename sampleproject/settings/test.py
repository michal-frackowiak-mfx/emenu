from .base import *  # noqa

DEBUG = True
SECRET_KEY = '5kynhy(y2odmr(74m(mtk+%9@&8dbms@5d@!=+l&rr01(6hoh2'

INSTALLED_APPS += (  # noqa: F405
    'django_nose',
    'test_pep8'
)

PROJECT_DIR = os.path.dirname(__file__)
TEST_PEP8_DIRS = [
    os.path.dirname(PROJECT_DIR),
    os.path.join(BASE_DIR)
]
TEST_PEP8_EXCLUDE = ['migrations', ]  # Exclude this paths from tests
TEST_PEP8_IGNORE = [
    # 'E128',
]  # Ignore this tests

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--with-coverage',
    '--cover-package=emenu',
    '-s',
    '--verbosity=2',
]
