from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import login
from django.http import HttpResponseRedirect
from django.urls import (
    path,
    re_path,
    include
)
from material.frontend import urls as frontend_urls


urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    re_path(r'^$', lambda r: HttpResponseRedirect('/emenu')),
    re_path(
        r'^accounts/login/$',
        login,
        {'template_name': 'emenu/registration/login.html'},
        name='login'
    ),
    path('admin/', admin.site.urls),
    path(r'', include(frontend_urls)),
] + static(
    settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT
)
